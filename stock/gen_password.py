import random

# code = input("Type any word/letter: ")
alphabet = 'AbCdEfGhIgKlMnOpQrStUvWxYz12-34567890'
rand_amount = random.randint(8, 18)
password = ''

for i in range(rand_amount):
    rand_symb = random.randint(0, 36)
    password += alphabet[rand_symb]

print(password)
