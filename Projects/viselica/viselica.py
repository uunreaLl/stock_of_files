word = input("Загадайте слово: ")
full_word = []
space_word = []
list_found_letters = []

for i in word:
    if i == " ":
        space_word += " "
        full_word += " "
    else:
        space_word += "_"
        full_word += i

word_1 = [i for i in word]

# print("\n", space_word, sep='')


def checking(letter, space_word, word1):
    if letter == "сдаюсь":
        print("Загаданное слово:", word)
    elif letter == word1:
        print("Congraz!")
        return 1
    elif len(letter) > 1:
        print("Вы можете ввести только одну букву")
        return 0
    elif letter.isdigit():
        print("Вы можете ввести только буквы")
        return 0
    elif letter in space_word:
        print("Вы уже отгадали эту букву")
        return 0


def guessed_word(space_word):
    word = ""
    for i in space_word:
        word += i
    return word


counter = 1

while "_" in space_word:
    letter = input("Введите букву: ").lower()
    result_checking = checking(letter, space_word, word)
    if result_checking == 0:
        continue
    elif result_checking == 1:
        break

    if letter in full_word:
        for found_letter in full_word:
            if found_letter == letter:
                index_letter = full_word.index(letter)
                list_found_letters.append(index_letter)
                full_word[index_letter] = "-"
                print(list_found_letters)
        for a in list_found_letters:
            a = int(a)
            space_word[a] = word_1[a]
        list_found_letters = []
        print(guessed_word(space_word))
    elif letter not in full_word:
        body = open(f'{counter}', 'r')
        print(body.read())
        if counter == 8:
            break
        else:
            counter += 1

# print(full_word, "\n" + str(space_word))
